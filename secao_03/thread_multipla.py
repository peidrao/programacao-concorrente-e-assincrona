import threading
import time


def main():
    threads = [
        threading.Thread(target=contar, args=('elefeante', 10)),
        threading.Thread(target=contar, args=('leao', 5)),
        threading.Thread(target=contar, args=('gaviao', 2)),
        threading.Thread(target=contar, args=('formiga', 15))
    ]

    [th.start() for th in threads]  # adiciona a nossa thread na pool de threads

    print('Podemos fazer outras coisas no programa')
    print('Geek University ' * 2)
    [th.join() for th in threads]  # avisa para ficar aguardando aqui até a thread terminar
    print('Pronto')  # Só será executado quando a thread finalizar


def contar(animal, numero):
    for n in range(1, numero+1):
        print(f'{n}: {animal}')
        time.sleep(1)


if __name__ == '__main__':
    main()
