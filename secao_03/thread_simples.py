import threading
import time


def main():
    th = threading.Thread(target=contar, args=('elefeante', 10))

    th.start()  # adiciona a nossa thread na pool de threads

    print('Podemos fazer outras coisas no programa')
    print('Geek University ' * 2)
    th.join()  # avisa para ficar aguardando aqui até a thread terminar
    print('Pronto')  # Só será executado quando a thread finalizar


def contar(animal, numero):
    for n in range(1, numero+1):
        print(f'{n}: {animal}')
        time.sleep(1)


if __name__ == '__main__':
    main()
