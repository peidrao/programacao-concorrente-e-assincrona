# Programação Concorrente e Assíncrona com Python

## Bibliotecas Async IO

A biblioteca requests não suporta assincronia. Uma substituta seria aiohttp

- [aiohttp](https://docs.aiohttp.org/en/stable/)


O mesmo acontece para a psycopg2 e uma alternativa é a aiopg

- [aiopg](https://aiopg.readthedocs.io/en/stable/)