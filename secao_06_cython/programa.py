import cumprimentar

def main():
    nome: str = input('Qual é seu nome? ')
    cumprimentar.cumprimentar(nome)


if __name__ == '__main__':
    main()