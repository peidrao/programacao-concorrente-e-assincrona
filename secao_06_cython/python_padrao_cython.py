import datetime
import computar

def main():
    inicio = datetime.datetime.now()
    computar.computar(fim=50_000_000)

    tempo = datetime.datetime.now() - inicio
    print(f'Terminou em {tempo.total_seconds():.2f} segundos')


if __name__ == '__main__':
    main()



# Terminou em 21.60 segundos
# Terminou em 18.23 segundos
# Terminou em 0.35 segundos
