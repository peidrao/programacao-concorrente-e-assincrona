import asyncio


async def hello_world():
    print('Hello')
    await asyncio.sleep(2)
    print('World')


async def hello_python():
    print('Python is...')
    await asyncio.sleep(4)
    print('the best!')


el = asyncio.get_event_loop()
el.run_until_complete(hello_world())
el.run_until_complete(hello_python())
el.close()