import asyncio


async def hello_world():
    print('hello world!')


el = asyncio.get_event_loop()
el.run_until_complete(hello_world())
el.close()