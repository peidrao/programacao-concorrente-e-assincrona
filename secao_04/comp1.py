import multiprocessing


def ping(cnx):
    cnx.send('Ping')


def pong(cnx):
    msg = cnx.recv()
    print(f'{msg} Pong')


def main():
    cnx1, cnx2 = multiprocessing.Pipe(True)

    p1 = multiprocessing.Process(target=ping, args=(cnx1,))
    p2 = multiprocessing.Process(target=pong, args=(cnx2,))

    p1.start()
    p2.start()

    p1.join()
    p2.join()


if __name__ == '__main__':
    main()
